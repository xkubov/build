"""builder

Todo:
    * create configure+make builder
    * split build and install
"""

from abc import (
    abstractmethod,
    ABC
)
import json
import multiprocessing
import os
import re
import subprocess
import sys
import threading

from progress.bar import Bar
from progress.spinner import Spinner


class Builder(ABC):
    def __init__(self, project_path):
        self._project = project_path
        if not self.__class__.can_build(project_path):
            raise FileNotFoundError(
                    'cannot build repository with this method.')

    def create_builder(project):
        if CMakeBuilder.can_build(project):
            return CMakeBuilder(project)

        return None

    @abstractmethod
    def can_build(project):
        pass

    @abstractmethod
    def _build(self, **kwargs):
        pass

    def _prepare_build(self, build_dir=None):
        if build_dir:
            if not os.path.isdir(build_dir):
                os.makedirs(build_dir, exist_ok=True)

    def _extract_config(self, config):
        with open(config) as json_file:
            return json.load(json_file)

    def build(self, build_dir='build', **kwargs):
        self._prepare_build(build_dir)
        kwargs['build_dir'] = build_dir

        if 'config' not in kwargs:
            return self._build(**kwargs)

        else:
            conf_args = self._extract_config(kwargs['config'])
            return self._build(**{**conf_args, **kwargs})

    def _print_stdout(self, stdout):
        for line in iter(stdout, b''):
            print(line.decode('utf-8').strip())

    def _print_stderr(self, stderr):
        for line in iter(stderr, b''):
            sys.stderr.write(line.decode('utf-8').strip())

    def _execute(self, cmd):
        p = subprocess.Popen(
                cmd,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                bufsize=1,
                close_fds=True
        )

        x = threading.Thread(target=self._print_stdout, args=(p.stdout.readline,))
        x.start()
        y = threading.Thread(target=self._print_stderr, args=(p.stderr.readline,))
        y.start()

        x.join()
        y.join()

        while p.poll() is None:
            # Process hasn't exited yet, let's wait some
            time.sleep(0.5)

        if p.returncode:
            print()
            sys.exit(p.returncode)

        p.stdout.close()
        p.wait()


class CMakeBuilder(Builder):
    def _tr_specials(self, specials):
        opts = []
        for key, value in specials.items():
            opts.append('-D'+key.upper()+'='+value)

        return opts

    def _tr_options(self, options):
        options_eq = {
            'debug': ('-DCMAKE_BUILD_TYPE', 'Debug'),
            'install_prefix': '-DCMAKE_INSTALL_PREFIX'
        }

        opts = []
        for key, value in options.items():
            if key in options_eq:
                kelem = options_eq[key]
                if isinstance(kelem, tuple):
                    if value:
                        k, v = kelem
                        opts.append(k+'='+v)
                else:
                    opts.append(kelem+'='+value)

        if 'special' in options:
            return [*opts, *self._tr_specials(options['special'])]

        return opts

    def can_build(project):
        return os.path.isfile(
            os.path.join(project, "CMakeLists.txt")
        )

    def _print_stdout(self, stdout):
        for line in iter(stdout, b''):
            outline = line.decode('utf-8').strip()
            pc = re.search(r'^.*\[(?: *)?(\d+)%\].* ([^/ ]+)$', outline)
            if pc and self._bar:
                self._bar.suffix = '%(index)d/%(max)d ({})'.format(pc.group(2))
                self._bar.goto(int(pc.group(1)))

            elif self._spinner:
                self._spinner.next()

    def _build(self, **kwargs):
        opts = self._tr_options(kwargs)
        os.chdir(kwargs['build_dir'])
        cmd = ['cmake', self._project, *opts]
        self._spinner = Spinner('cmake ')
        self._execute(cmd)
        self._spinner = None
        self._bar = Bar('Build', max=100)
        self._execute([
            'make',
            '-j'+str(multiprocessing.cpu_count())
        ])
        self._bar = Bar('Install', max=100)
        self._execute([
            'make',
            'install' ]
        )
        self._bar.finish()

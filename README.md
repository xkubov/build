# build

This repository contains build script that I use for building projects that
use cmake. This script comes handy when I need to have project built
several times for example when I work on more branches.

## Usage

```bash
usage: build.py [-h] [-p PROJ_DIR] [-b DIR] [-d] [-c CONFIG] [--no-git]

optional arguments:
  -h, --help                        how this help message and exit
  -p PROJ_DIR, --project PROJ_DIR   project directory
  -b DIR, --build-dir DIR           build directory
  -d, --debug                       build in debug mode (default: False)
  -c CONFIG, --config CONFIG        use build configuration from file
  --no-git                          turns off usage of git data
```

## Todo:

* Bad output with small screen.
* Optional install.
* Optional only install.
* Optional preface (cmake).

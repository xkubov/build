#!/usr/bin/env python3

import argparse
import os
import sys

import config

from info import git
from build.builder import Builder


def panic(*args):
    print('Error:', *args)
    sys.exit(1)


def warning(*args):
    print('Warning:', *args)


def parse_args(args):
    parser = argparse.ArgumentParser(
                description=__doc__,
                formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', '--project',
                        type=str,
                        dest='project', default="",
                        metavar='PROJ_DIR',
                        help='Project directory.')

    parser.add_argument('-b', '--build-dir',
                        type=str,
                        dest='build_dir', default="build",
                        metavar='DIR',
                        help='Build directory.')

    parser.add_argument('-d', '--debug',
                        dest='debug', default=False,
                        action='store_true',
                        help='Build in debug mode')

    parser.add_argument('-c', '--config',
                        type=str,
                        dest='config', default=None,
                        help='Use build configuration from file.')

    parser.add_argument('--no-git',
                        dest='git', default=True,
                        action='store_false',
                        help='turns of usage of git data.')

    return parser.parse_args(args)


def check_args(args):
    if not args.project:
        args.project = os.getcwd()

    elif not os.path.isdir(args.project):
        project = os.path.join(config.PROJECT_DIR, args.project)
        if os.path.isdir(project):
            args.project = project
        else:
            raise FileNotFoundError('project does not exist: '+project)

    if args.git:
        try:
            gitinfo = git.GitInfo(args.project)
            args.project = gitinfo.project_path()
            args.build_dir = os.path.join(args.build_dir, gitinfo.branch())

        except Exception as e:
            warning(str(e))
            if not os.path.isdir(args.project):
                panic('not a directory:', args.project)

    # This makes path relative to project dir - NOT cwd
    if not os.path.isabs(args.build_dir):
        args.build_dir = os.path.join(args.project, args.build_dir)

    if args.config:
        if not os.path.isfile(args.config):
            abs_conf = os.path.join(args.project, args.config)
            if not os.path.isfile(abs_conf):
                panic('file does not exist:', args.config)

            else:
                args.config = abs_conf
    else:
        abs_conf = os.path.join(args.project, '.config.json')
        if os.path.isfile(abs_conf):
            args.config = abs_conf


def check_config():
    if config.PROJECT_DIR:
        if not os.path.isdir(config.PROJECT_DIR):
            raise FileNotFoundError(
                    "project direcotry does not exist: "+config.PROJECT_DIR
            )


if __name__ == "__main__":
    try:
        check_config()
        args = parse_args(sys.argv[1:])
        check_args(args)

        builder = Builder.create_builder(args.project)
        if not builder:
            panic('cannot find a propper method to build the project')

        build_args = {
                'build_dir': args.build_dir,
        }

        if args.debug:
            build_args['debug'] = args.debug
        if args.config:
            build_args['config'] = args.config

        builder.build(**build_args)

    except Exception as e:
        panic(str(e))

import os
import subprocess
from subprocess import PIPE


class GitInfo():
    def __init__(self, repo=None):
        if not repo:
            repo = os.getcwd()

        self._check_repo_extract_info(repo)

    def _git_execute(self, *args):
        gitout = subprocess.run(
            ['git', *args],
            stdout=PIPE, stderr=PIPE
        )
        if gitout.returncode:
            raise FileNotFoundError(
                gitout.stderr.decode(
                    'UTF-8'
                ).strip().replace(
                    'fatal: ', '')
            )

        return gitout.stdout.decode('UTF-8').strip()

    def _check_repo_extract_info(self, repo):
        if not os.path.isdir(repo):
            raise FileNotFoundError("not directory: %s" % repo)

        old_path = os.getcwd()
        os.chdir(repo)

        self._repo = self._git_execute(
            'rev-parse',
            '--show-toplevel'
        )
        self._branch = self._git_execute(
            'rev-parse',
            '--abbrev-ref',
            'HEAD'
        )

        os.chdir(old_path)

    def project_path(self):
        return self._repo

    def branch(self):
        return self._branch
